<!DOCTYPE html>
<html lang='vn'>

<head>
    <meta charset='UTF-8'>
</head>
<title>Login</title>

<body>

    <fieldset style='width: 600px; height: 600px; border:#1de416 solid'>
        <?php
        session_start();

        include("connection.php");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $gender = ($_SESSION["gender"]) == "Nam" ? 1 : 0;
            $faculty = ($_SESSION["faculty"] == "Khoa học máy tính" ? "MAT" : "KDL");
            $_query = "INSERT INTO students (name, gender, faculty, birthday, address, avatar) VALUES('" . $_SESSION["name"] . "','" . $gender . "','" . $faculty . "','" . $_SESSION["dob"] . "','" . $_SESSION["address"] . "','" . $_SESSION["filename"] . "')";
            echo $_query;
            $result = $conn->query($_query);
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            header("location: web_success.php");
        }
        ?>
        <form style='margin: 50px 70px 0 50px' method="post">
            <table style='border-collapse: separate; border-spacing: 15px 15px;'>
                <tr height='40px'>
                    <td style='background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                        <label style='color: white;'>Họ và tên</label>
                    </td>
                    <td>
                        <?php
                        echo "<div style='color: black;'>" . $_SESSION["name"] . "</div>"
                        ?>
                    </td>
                </tr>
                <tr height='40px'>
                    <td width=40% style='background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                        <label style='color: white;'>Giới tính</label>
                    </td>
                    <td>
                        <?php
                        echo "<div style='color: black;'>" . $_SESSION["gender"] . "</div>"
                        ?>
                    </td>
                </tr>
                <tr height='40px'>
                    <td style='background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                        <label style='color: white;'>Phân Khoa</label>
                    </td>
                    <td height='40px'>
                        <?php
                        echo "<div style='color: black;'>" . $_SESSION["faculty"] . "</div>"
                        ?>
                    </td>
                </tr>

                <tr height='40px'>
                    <td style='background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                        <label style='color: white;'>Ngày sinh</label>
                    </td>
                    <td height='40px'>
                        <?php

                        $dayOfBirth = $_SESSION["dob"];
                        echo "<div style='color: black;'>" . date("d-m-Y", strtotime($dayOfBirth)) . "</div>"
                        ?>
                    </td>
                </tr>

                <tr height='40px'>
                    <td style='background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                        <label style='color: white;'>Địa chỉ</label>
                    </td>
                    <td height='40px'>
                        <?php
                        echo "<div style='color: black;'>" . $_SESSION["address"] . "</div>"
                        ?>
                    </td>
                </tr>

                <tr height='100px'>
                    <td style='background-color: #1de416; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                        <label style='color: white;'>Hình ảnh </label>
                    </td>
                    <td height='100px'>
                        <?php
                        echo "<img src='" . $_SESSION["filename"] . "' width='100' height='100'>";
                        ?>
                    </td>
                </tr>

            </table>
            <button style='background-color: #49be25; border-radius: 10px; 
        width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Xác nhận</button>
        </form>

    </fieldset>
</body>

</html>