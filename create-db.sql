CREATE DATABASE tuan12 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS tuan12.students
(
    name varchar(30),
    gender varchar(1),
    faculty varchar(3),
    birthday DateTime,
    address varchar(30),
    avatar varchar(100)
);